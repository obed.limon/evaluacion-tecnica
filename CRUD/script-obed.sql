CREATE DATABASE [EvaluacionTecnica]

USE [EvaluacionTecnica]
GO
/****** Object:  Table [dbo].[Empleados]    Script Date: 17/08/2022 11:19:01 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empleados](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](150) NOT NULL,
	[RFC] [varchar](13) NULL,
	[CURP] [varchar](18) NULL,
 CONSTRAINT [PK_Empleados] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Empleados] ON 

INSERT [dbo].[Empleados] ([Id], [Nombre], [RFC], [CURP]) VALUES (1, N'Obed Limon', N'LIPO880725KX3', N'LIPO880725HCHMRB00')
SET IDENTITY_INSERT [dbo].[Empleados] OFF
GO
