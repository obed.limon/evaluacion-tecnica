﻿using CRUD.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CRUD.Controllers
{
    public class HomeController : Controller
    {
        private EvaluacionTecnicaEntities db = new EvaluacionTecnicaEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public JsonResult GetEmpleados()
        {
            JsonResult respuesta;
            try
            {
                var lista = db.Empleados.ToList();
                if (lista != null)
                {
                    var listaFinal = lista.Select(l => new
                    {
                        Id = l.Id,
                        Nombre = l.Nombre,
                        RFC = l.RFC,
                        CURP = l.CURP
                    }).ToList();
                    respuesta = Json(listaFinal, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    respuesta = Json(null);
                }
            }
            catch (Exception)
            {
                respuesta = Json(new { Estado = "ERROR" });
            }
            return respuesta;
        }

        public ActionResult CrearEmpleado()
        {
            var modelo = new Empleados();
            return View(modelo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CrearEmpleado([Bind(Include = "Id,Nombre,RFC,CURP")] Empleados empleados)
        {
            if (ModelState.IsValid)
            {
                db.Empleados.Add(empleados);
                db.SaveChanges();
                return RedirectToAction("/Index");
            }

            return View(empleados);
        }

        public ActionResult EditarEmpleado(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var modelo = db.Empleados.Find(id);
            if (modelo == null)
                return HttpNotFound();

            return View(modelo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarEmpleado([Bind(Include = "Id,Nombre,RFC,CURP")] Empleados empleados)
        {
            if (ModelState.IsValid)
            {
                db.Entry(empleados).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("/Index");
            }
            return View(empleados);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            JsonResult result = null;
            try
            {
                var empleado = db.Empleados.Find(id);
                if (empleado != null)
                {
                    db.Empleados.Remove(empleado);
                    db.SaveChanges();
                }
                else
                    result = Json(new { r = "Error" });

                result = Json(new { r = "OK" });
            }
            catch (Exception)
            {
                result = Json(new { r = "Error" });
            }
            return result;
        }
    }
}