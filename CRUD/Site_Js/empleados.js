﻿$(document).ready(function () {
    $('#codeId').keypress(function (event) {
        var keycode = event.keyCode ? event.keyCode : event.which;
        if (keycode === 13) {
            $("#descriptionId").focus();
        }
    });
    $('#descriptionId').keypress(function (event) {
        var keycode = event.keyCode ? event.keyCode : event.which;
        if (keycode === 13) {
            $("#btnSave").click();
        }
    });
    LoadTable();
});
var table;
function LoadTable() {
    table = $('#tblEmpleados').DataTable({
        "ordering": false,
        ajax: {
            url: "/Home/GetEmpleados/",
            type: "GET",
            async: false,
            dataType: "json",
            beforeSend: function () {
            },
            dataSrc: function (json) {
                return json;
            }
        },
        columns: [
            { "data": "Id", "title": "Id" },
            { "data": "Nombre", "title": "Nombre" },
            { "data": "RFC", "title": "RFC" },
            { "data": "CURP", "title": "CURP" },
            {
                "title": "Opciones",
                "render": function (data, type, full, meta) {
                    return "<a href = '/Home/EditarEmpleado/" + full.Id + "' class='btn btn-success' ><i class='material-icons align-middle'>edit</i>&nbsp;Edit</a > " + " " +
                        "<a href='javascript:void(0);'" + full.Id + "' onclick='Delete(" + full.Id + ")' class='btn btn-danger' ><i class='material-icons align-middle'>clear</i>&nbsp;Eliminar</a>";
                }
            }
        ]
    });
}

function Delete(id) {
    $("#modal").modal("show");
    $("#valueDelete").val(id);
}

function deleteRegister() {
    var deleteId = $("#valueDelete").val();
    $("#modal").modal("hide");
    $.ajax({
        url: '/Home/Delete/' + deleteId,
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        data: {},
        success: function (response) {
            if (response.r === "OK") {
                $.toast({
                    heading: 'Elminado',
                    text: 'El empleado fue eliminado correctamente',
                    icon: 'success'
                });
                table.ajax.reload();
            } else {
                $.toast({
                    heading: 'ERROR!',
                    text: 'No es posible eliminar el empleado. intente de nuevo o contacte al administrador',
                    icon: 'error'
                });
            }
        }, error: function (error) {
            console.log(error);
            $.toast({
                heading: 'ERROR!',
                text: 'No es posible eliminar el empleado. intente de nuevo o contacte al administrador',
                icon: 'error'
            });
        }
    });
}
